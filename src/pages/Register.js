import { Form, Button } from 'react-bootstrap';
import { useState,useEffect } from 'react';
import { Navigate, useNavigate } from "react-router-dom";
import { useContext } from 'react';
import UserContext from '../UserContext';
import "./Register.css";
import img1 from '../images/purrfect.jpg';
import Swal from "sweetalert2";

export default function Register(){

    const { user } = useContext(UserContext)
    
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] =useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const navigate = useNavigate();
    
    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(mobileNumber);
    console.log(email);
    console.log(password1);
    console.log(password2);

    function registerUser(event){ 
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {"Content-Type" : "application/json"},
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNumber: mobileNumber,
                email: email,
                password: password1
            })
                
        })
        .then(res => res.json())
        .then(data => {
        
        console.log("Register User Data");
        console.log(data)
            
            if(data){

                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Pawsitively Purrfect!"
                })
                navigate("/login")

                
            }
            else{
                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Please provide a different email"
                })
            }
        })
    }
    useEffect(() => {
        if ((email!=='' && password1!=='' && password2 !== '' && mobileNumber.length >=11 ) && (password1 ===  password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNumber, email, password1, password2])

    return(

    (user.id !== null)
    ?
    <Navigate to="/products" />
    :
    <div className="image-container1" >
        
        <img src ={img1} alt= "" className='img-fluid'/>
        
        <Form className='registration-form' onSubmit={(event) => registerUser(event)}>
                <h1 className='text-center'>Register</h1>
                <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter your first name" 
                            value={firstName}
                            onChange={event => setFirstName(event.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter your last name" 
                            value={lastName}
                            onChange={event => setLastName(event.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId="mobileNumber">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter your mobile number" 
                            value={mobileNumber}
                            onChange={event => setMobileNumber(event.target.value)}
                            required
                        />
                    </Form.Group>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control className='control' 
                        type="email" 
                        placeholder="Enter your email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        Your email address will remain confidential and will not be shared with any third party.
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password1}
                        onChange = {event => setPassword1(event.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password"
                        value = {password2} 
                        onChange = {e=> setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>
                { isActive ? 
                    <Button variant="primary" type="submit" id="submitButton">
                    Register
                    </Button>
                    : 
                    <Button variant="primary" type="submit" id="submitButton"
                        disabled>
                    Register
                    </Button>
                    
                
                }
                
        </Form>
    </div>
    )
}
